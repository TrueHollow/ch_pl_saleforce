# Plaid with Salesforce

## System requirements:

Node.js version 12 or higher. 

## Prepare

`npm i --production`

## Enviroment variables

```dotenv
PLAID_CLIENT_ID=
PLAID_SECRET=
PLAID_PUBLIC_KEY=
PLAID_ENV=
PLAID_PRODUCTS=assets,auth,identity,income,investments,liabilities,transactions
PLAID_COUNTRY_CODES=US,CA,GB,FR,ES

SALESFORCE_USERNAME=
SALESFORCE_PASSWORD=
SALESFORCE_SECURITY_TOKEN=

CRYPTO_KEY=
CRYPTO_IV=

SENDGRID_API_KEY=some_key
APP_BASE_URL=https://ch-pl-saleforce.herokuapp.com
EMAIL_FROM_ADDRESS=example@example.com
```

## Run

`npm start`
