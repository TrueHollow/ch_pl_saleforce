module.exports = {
  /**
   * Logger common configuration
   */
  log4js: {
    appenders: {
      out: {
        type: 'stdout',
      },
    },
    categories: {
      default: {
        appenders: ['out'],
        level: 'debug',
      },
    },
  },
  /**
   * Default express http port
   */
  express: {
    http_port: 3000,
  },
  cron: {
    /**
     * Schedule for daemon task
     */
    updateAssetsTask: {
      schedule: '0 0 0 * * *',
    },
  },
  tokens: {
    /**
     * 15 minutes
     */
    EXPIRATION_TIME: 1000 * 60 * 15,
  },
  SalesForce: {
    options: {
      // https://jsforce.github.io/document/#connection
      // you can change loginUrl to connect to sandbox or prerelease env.
      // loginUrl : 'https://test.salesforce.com'
    },
  },
};
