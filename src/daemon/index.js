const { CronJob } = require('cron');
const config = require('../config');
const logger = require('../common/Logger')('src/daemon/index.js');
require('../common/dotEnv_proxy');
const CryptoProvider = require('../service/CryptoProvider');
const Backend = require('../service/Backend');
const MailSending = require('../service/MailSending');

logger.debug('Script is started.');

const sendMailToClient = async userId => {
  try {
    const [client] = await Backend.SalesProvider.getClientByUserId(userId);
    if (!client) {
      logger.warn('Strange. client for userId (%s) not found', userId);
      return;
    }
    const { email__c: email } = client;
    await MailSending.invalidTokenIssue({ email });
  } catch (e) {
    logger.error(e);
  }
};

/**
 * Process Tokens object (filtered by isValid: true)
 * @param dbTokenObject Tokens object
 * @returns {Promise<{ err, result }>}
 */
const processItem = async dbTokenObject => {
  let err;
  let result;
  try {
    logger.debug(`Processing: ${JSON.stringify(dbTokenObject)}`);
    const { token__c: token } = dbTokenObject;
    const decryptedToken = CryptoProvider.decrypt(token);
    await Backend.pushAssetsDataFromPlaidToSalesforce(decryptedToken);
    await Backend.SalesProvider.updateTokens(
      {
        updated_at__c: new Date(),
      },
      dbTokenObject.Id
    );
    result = true;
  } catch (e) {
    if (e.error_code === 'INVALID_ACCESS_TOKEN') {
      logger.warn(
        `Token (${dbTokenObject.token}) for user id: ${dbTokenObject.userId} (db id: ${dbTokenObject.id}) is INVALID. Updating flag on DB.`
      );
      await Backend.SalesProvider.updateTokens(
        {
          isValid__c: false,
          updated_at__c: new Date(),
        },
        dbTokenObject.Id
      );
      await sendMailToClient(dbTokenObject.userId__c);
    } else {
      logger.error(
        `Cron error, processing using token for user id: ${dbTokenObject.userId} (db id: ${dbTokenObject.id})`
      );
      logger.error(e);
    }
    err = e;
  }
  logger.debug('Processing finished.');
  return { err, result };
};

/**
 * Cron entry point (invoking by schedule).
 * @returns {Promise<void>}
 */
const main = async () => {
  logger.info('Main: invoked.');
  await Backend.init();
  const validTokens = await Backend.SalesProvider.getOldValidTokens();

  // The sequential order
  // eslint-disable-next-line no-restricted-syntax
  for (const tokenObject of validTokens) {
    // eslint-disable-next-line no-await-in-loop
    await processItem(tokenObject);
  }
};

/**
 * Cron job
 */
const job = new CronJob(
  config.cron.updateAssetsTask.schedule,
  main,
  null,
  false,
  null,
  null,
  false
);
job.start();

module.exports = {
  processItem,
};
