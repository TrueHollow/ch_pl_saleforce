require('./common/dotEnv_proxy');
const path = require('path');
const express = require('express');
const middleware = require('./middleware');
const routes = require('./routes');
const config = require('./config');
const logger = require('./common/Logger')('src/index.js');

/**
 * Express application
 * @type {app}
 */
const app = express();

app.use('/static', express.static(path.join(__dirname, 'static')));
middleware(app, logger);
routes(app);

const httpPort = process.env.PORT || config.express.http_port;

const listener = app.listen(httpPort);
logger.info(`app running on port http://localhost:${httpPort} ...`);

module.exports = { app, listener };
