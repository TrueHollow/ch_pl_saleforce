const path = require('path');
const env = require('env-var');
const Backend = require('../../service/Backend');
const config = require('../../config');

const PLAID_PUBLIC_KEY = env
  .get('PLAID_PUBLIC_KEY')
  .required()
  .asString();
const PLAID_ENV = env
  .get('PLAID_ENV')
  .required()
  .asString();
const PLAID_PRODUCTS = env
  .get('PLAID_PRODUCTS')
  .required()
  .asString();
const PLAID_COUNTRY_CODES = env
  .get('PLAID_COUNTRY_CODES')
  .required()
  .asString();

/**
 * Token will expire in 15 minutes
 * @type {number}
 */
const TOKEN_EXPIRATION_TIME = config.tokens.EXPIRATION_TIME;

/**
 * Set EJS route for HTML responses.
 * @param app Express application
 */
module.exports = app => {
  const SRC_ROOT = path.resolve(__dirname, '..', '..');
  app.set('views', path.join(SRC_ROOT, 'views'));
  app.set('view engine', 'ejs');

  app.get('/bind/:salesforceToken', async (req, res, next) => {
    const { salesforceToken } = req.params;
    const [client] = await Backend.SalesProvider.getClientByToken(
      salesforceToken
    );
    if (!client) {
      return next();
    }
    const timestamp = Date.parse(client.timestamp__c);

    if (Date.now() - timestamp > TOKEN_EXPIRATION_TIME) {
      return res.redirect('/invalid_token');
    }

    return res.render('ui_bind', {
      title: 'On The Road Lending: Finish your Application',
      PLAID_PUBLIC_KEY,
      PLAID_ENV,
      PLAID_PRODUCTS,
      PLAID_COUNTRY_CODES,
      salesforceToken,
    });
  });

  app.get('/upload_success', (req, res) => {
    res.render('uploadSuccess', {
      title: 'On The Road Lending: Success',
    });
  });

  app.get('/link_success', (req, res) => {
    res.render('link_success', {
      title: 'On The Road Lending: Success',
    });
  });

  app.get('/link_failure', (req, res) => {
    res.render('link_failure', {
      title: 'On The Road Lending: Error',
    });
  });

  app.get('/api', (req, res) => {
    res.render('api', {
      title: 'Api testing',
      PLAID_PUBLIC_KEY,
      PLAID_ENV,
      PLAID_PRODUCTS,
      PLAID_COUNTRY_CODES,
    });
  });
};
