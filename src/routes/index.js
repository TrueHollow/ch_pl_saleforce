const logger = require('../common/Logger')('src/routes/index.js');
const ejs = require('./ejs');
const uploadRoute = require('./upload');
const Backend = require('../service/Backend');
const config = require('../config');

/**
 * Token will expire in 15 minutes
 * @type {number}
 */
const TOKEN_EXPIRATION_TIME = config.tokens.EXPIRATION_TIME;

/**
 * Set all Express application routes
 * @param app Express application
 */
module.exports = app => {
  ejs(app);
  uploadRoute(app);

  /**
   * Main route. Used for binding data from Plaid to Salesforce and connect with userId
   */
  app.post('/assets/data', async (req, res, next) => {
    try {
      const { publicToken, salesforceToken } = req.body;
      const [client] = await Backend.SalesProvider.getClientByToken(
        salesforceToken
      );
      if (!client) {
        return next();
      }
      const timestamp = Date.parse(client.timestamp__c);
      if (Date.now() - timestamp > TOKEN_EXPIRATION_TIME) {
        return res.redirect('/invalid_token');
      }
      const { Id: applicationId } = client;
      const authResult = await Backend.PlaidProvider.getAccessToken(
        publicToken
      );
      const accessToken = authResult.access_token;
      const [{ assetReportGetResponse }] = await Promise.all([
        Backend.pushAssetsDataFromPlaidToSalesforce(accessToken, applicationId),
        Backend.SalesProvider.saveTokenForUserId(accessToken, applicationId),
      ]);
      const result = {
        json: assetReportGetResponse.report,
      };
      return res.json(result);
    } catch (e) {
      logger.error(e);
      return res.status(500).json({ error: e });
    }
  });

  /**
   * Custom handler HTTP 404 error.
   */
  app.use((req, res) => {
    res.status(404);

    // respond with html page
    if (req.accepts('html')) {
      return res.render('404', { url: req.url });
    }

    // respond with json
    if (req.accepts('json')) {
      return res.send({ error: 'Not found' });
    }

    // default to plain-text. send()
    return res.type('txt').send('Not found');
  });
};
