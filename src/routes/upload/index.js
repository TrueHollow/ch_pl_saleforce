const multer = require('multer');
const logger = require('../../common/Logger')('src/routes/uploadRoute.js');
const Backend = require('../../service/Backend');
const config = require('../../config');

const salesProvider = Backend.SalesProvider;
/**
 * Temporary storage for uploaded files.
 * @type {MemoryStorage}
 * @deprecated Should change to file storage in production
 */
const storage = multer.memoryStorage();

/**
 * Maximum files for uploading (LIMIT)
 * @type {number}
 */
const MAX_DOCUMENTS_COUNT = 3;

/**
 * Maximum file size for uploading (LIMIT)
 * @type {number}
 */
const MAX_FILESIZE = 5 * 1024 * 1024;

/**
 * Token will expire in 15 minutes
 * @type {number}
 */
const TOKEN_EXPIRATION_TIME = config.tokens.EXPIRATION_TIME;

/**
 * Upload handler
 */
const upload = multer({ storage, limits: { fileSize: MAX_FILESIZE } }).array(
  'userDocuments',
  MAX_DOCUMENTS_COUNT
);

/**
 * Set route for uploading files to Salesforce.
 * @param app
 */
module.exports = app => {
  app.post('/upload', (req, res, next) => {
    upload(req, res, async err => {
      try {
        if (err) {
          throw err;
        }
        const { body, files } = req;
        const { salesforceToken } = body;
        const [client] = await Backend.SalesProvider.getClientByToken(
          salesforceToken
        );
        if (!client) {
          return next();
        }
        const timestamp = Date.parse(client.timestamp__c);
        if (Date.now() - timestamp > TOKEN_EXPIRATION_TIME) {
          return res.redirect('/invalid_token');
        }
        const { Id: applicationId } = client;
        const ret = await salesProvider.createManualUploadObject(applicationId);
        await Promise.all(
          files.map(async file => {
            return salesProvider.uploadFileAsAttachment(
              ret.id,
              file.originalname,
              file.buffer,
              file.mimetype
            );
          })
        );
        return res.redirect('/upload_success');
      } catch (e) {
        logger.error(e);
        return res.status(500).json({ error: 'Error uploading file.' });
      }
    });
  });
};
