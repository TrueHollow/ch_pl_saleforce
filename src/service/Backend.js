const SalesforceProvider = require('./SalesforceProvider');
const PlaidProvider = require('./PlaidProvider');
const logger = require('../common/Logger')('src/service/Backend.js');
const config = require('../config');

/**
 * Handler for all app services
 */
class Backend {
  /**
   * Create Backend services
   */
  constructor() {
    this.salesProvider = new SalesforceProvider(config.SalesForce.options);
    this.plaidProvider = new PlaidProvider();
    this.isInit = null;
  }

  /**
   * Initialize Backend services
   * @returns {Promise<boolean>} Returns init state
   */
  async init() {
    if (!this.isInit) {
      this.isInit = this.salesProvider.connect();
      await this.isInit;
      logger.info('Connection to salesforce established.');
    }
    return this.isInit;
  }

  /**
   * Get salesProvider
   * @returns {SalesforceProvider}
   */
  get SalesProvider() {
    return this.salesProvider;
  }

  /**
   * Get plaidProvider
   * @returns {PlaidProvider}
   */
  get PlaidProvider() {
    return this.plaidProvider;
  }

  /**
   * Main app code. Process and bind plaid data to Salesforce
   * @param {string} token User auth token
   * @param {string} applicationId Id of Application record in Salesforce
   * @returns {Promise<{assetReportGetResponse: *, assetReportCreateResponse: *}>}
   */
  async pushAssetsDataFromPlaidToSalesforce(token, applicationId) {
    const assetReportCreateResponse = await this.plaidProvider.getAssets(token);
    const assetReportToken = assetReportCreateResponse.asset_report_token;
    const assetReportGetResponse = await this.plaidProvider.tryToGetAssetReport(
      assetReportToken
    );
    const [ret, assetReportGetPdfResponse] = await Promise.all([
      this.salesProvider.pushReportData(assetReportGetResponse, applicationId),
      this.plaidProvider.getAssetReportPdf(assetReportToken),
      this.salesProvider.pushAccountBasedData(
        assetReportGetResponse,
        applicationId
      ),
    ]);
    // https://github.com/jsforce/jsforce/issues/43
    await this.salesProvider.uploadFileAsAttachment(
      ret.id,
      'AssetsReport.pdf',
      assetReportGetPdfResponse.buffer,
      'application/pdf'
    );
    return {
      assetReportCreateResponse,
      assetReportGetResponse,
    };
  }
}

/**
 * Singleton
 * @type {Backend}
 */
const singleton = new Backend();
singleton.init().then(() => logger.info('Backend ready.'));

/**
 * Backend Singleton
 * @type {Backend}
 */
module.exports = singleton;
