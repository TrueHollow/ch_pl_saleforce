const crypto = require('crypto');
const env = require('env-var');

/**
 * Encrypt/Decrypt Algorithm
 * @type {string}
 */
const ALGORITHM = 'aes-256-cbc';

const CRYPTO_KEY = env
  .get('CRYPTO_KEY')
  .required()
  .asString();

const CRYPTO_IV = env
  .get('CRYPTO_IV')
  .required()
  .asString();

/**
 * CryptoKey
 * @type {Buffer}
 */
const cryptoKey = Buffer.from(CRYPTO_KEY, 'base64');
/**
 * CryptoVector
 * @type {Buffer}
 */
const cryptoIv = Buffer.from(CRYPTO_IV, 'base64');

class CryptoProvider {
  /**
   * Encrypt text
   * @param {string} text
   * @returns {string} String in base64
   */
  static encrypt(text) {
    const cipher = crypto.createCipheriv(ALGORITHM, cryptoKey, cryptoIv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return encrypted.toString('base64');
  }

  /**
   * Decrypt text
   * @param {string} text String in base64
   * @returns {string}
   */
  static decrypt(text) {
    const encryptedText = Buffer.from(text, 'base64');
    const decipher = crypto.createDecipheriv(ALGORITHM, cryptoKey, cryptoIv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
  }
}

module.exports = CryptoProvider;
