const knex = require('../db');
const CryptoProvider = require('./CryptoProvider');

/**
 * Minor handler for storing tokens in database using knex.
 */
class DbProvider {
  /**
   * Save token for user
   * @param {string} token
   * @param {string} userId
   * @returns {Promise<void>}
   */
  static async SaveToken(token, userId) {
    const dbData = {
      token: CryptoProvider.encrypt(token),
      userId,
    };
    await knex('tokens').insert(dbData);
  }

  /**
   * Get valid tokens
   * @returns {Promise<Array<Object>>}
   */
  static async GetValidTokens() {
    return knex('tokens')
      .select()
      .where('isValid', true);
  }

  /**
   * Update token in database
   * @param {Object} data
   * @param {string|number} id
   * @returns {Promise<void>}
   */
  static async UpdateTokens(data, id) {
    await knex('tokens')
      .update(data)
      .where('id', id);
  }
}

module.exports = DbProvider;
