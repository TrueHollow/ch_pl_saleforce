const sgMail = require('@sendgrid/mail');
const env = require('env-var');

const SENDGRID_API_KEY = env
  .get('SENDGRID_API_KEY')
  // .required() todo: Change it to required in the future
  .asString();

const APP_BASE_URL = env
  .get('APP_BASE_URL')
  // .required() todo: Change it to required in the future
  .asString();

const EMAIL_FROM_ADDRESS = env
  .get('EMAIL_FROM_ADDRESS')
  // .required() todo: Change it to required in the future
  .asString();

const DO_NOT_SEND_EMAIL = env.get('DO_NOT_SEND_EMAIL', 'false').asBool();

sgMail.setApiKey(SENDGRID_API_KEY);

/**
 * Wrapper for sending mails.
 */
class MailSending {
  /**
   * Send mail, if issue will appear
   * @param email
   * @returns {Promise<[Object, {}]|null>}
   */
  static async invalidTokenIssue({ email }) {
    // Useful for testing purposes.
    if (DO_NOT_SEND_EMAIL) {
      return null;
    }
    const msg = {
      to: email,
      from: EMAIL_FROM_ADDRESS,
      templateId: 'd-94140e6b47264aaca6fc3ad58d25762e',
      substitutions: {
        confirmation_link: `${APP_BASE_URL}/activate/`,
      },
      dynamic_template_data: {
        confirmation_link: `${APP_BASE_URL}/activate/`,
        subject: 'Account Verification',
        sender_name: 'Human Capable Inc',
        sender_address: '',
        sender_city: '',
        sender_state: '',
        sender_zip: '',
      },
      // subject: 'Please active your profile.',
      // text: `Activation link ${APP_BASE_URL}/activate/${uid}`,
      // html: `<a href="${APP_BASE_URL}/activate/${uid}">Activation link</a>`,
    };
    return sgMail.send(msg);
  }
}

module.exports = MailSending;
