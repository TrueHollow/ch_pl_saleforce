const jsforce = require('jsforce');
const env = require('env-var');
const Report = require('./models/Report');
const Account = require('./models/Account');
const HistoricalBalance = require('./models/HistoricalBalance');
const Owner = require('./models/Owner');
const Transaction = require('./models/Transaction');
const logger = require('../common/Logger')('src/service/SalesforceProvider.js');
const CryptoProvider = require('./CryptoProvider');

const SALESFORCE_USERNAME = env
  .get('SALESFORCE_USERNAME')
  .required()
  .asString();
const SALESFORCE_PASSWORD = env
  .get('SALESFORCE_PASSWORD')
  .required()
  .asString();
const SALESFORCE_SECURITY_TOKEN = env
  .get('SALESFORCE_SECURITY_TOKEN')
  .required()
  .asString();

logger.info(`SALESFORCE_USERNAME length: ${SALESFORCE_USERNAME.length}`);

logger.info(`SALESFORCE_PASSWORD length: ${SALESFORCE_PASSWORD.length}`);

logger.info(
  `SALESFORCE_SECURITY_TOKEN length: ${SALESFORCE_SECURITY_TOKEN.length}`
);

/**
 * Salesforce api wrapper.
 */
class SalesforceProvider {
  constructor(options = {}) {
    this.conn = new jsforce.Connection(options);
  }

  // https://poanchen.github.io/blog/2018/08/27/how-to-fix-login-must-use-security-token-in-Salesforce
  // https://na114.lightning.force.com/lightning/setup/NetworkAccess/page?address=%2F05G
  /**
   * Connect to salesforce or throw error;
   * @returns {Promise<void>}
   */
  async connect() {
    return new Promise((resolve, reject) => {
      const { conn } = this;
      conn.login(
        SALESFORCE_USERNAME,
        SALESFORCE_PASSWORD + SALESFORCE_SECURITY_TOKEN,
        (err, userInfo) => {
          if (err) {
            return reject(err);
          }
          // Now you can get the access token and instance URL information.
          // Save them to establish connection next time.
          logger.log(`Access token: ${conn.accessToken}`);
          logger.log(`Instance url: ${conn.instanceUrl}`);
          // logged in user property
          logger.log(`User ID: ${userInfo.id}`);
          logger.log(`Org ID: ${userInfo.organizationId}`);
          return resolve();
        }
      );
    });
  }

  /**
   * Push account based asset data
   * @param assetReportGetResponse
   * @param applicationId
   * @returns {Promise<void>}
   */
  async pushAccountBasedData(assetReportGetResponse, applicationId) {
    const promises = assetReportGetResponse.report.items.map(async item => {
      const { accounts } = item;
      const accountsPromises = accounts.map(async acc => {
        const account = new Account(acc, applicationId);
        const { id } = await this.createPlaidAccountObject(account);
        const histBalObject = new HistoricalBalance(acc, applicationId, id);
        const balances = histBalObject.Balances;
        const owner = new Owner(acc, applicationId, id);
        const transactions = acc.transactions
          .map(t => new Transaction(t, applicationId, id))
          .flat(1);
        const dataPromises = [
          this.createHistoricalBalanceObjects(balances),
          this.createOwnersObjects(owner),
          this.createTransactionsObjects(transactions),
        ];
        return Promise.all(dataPromises);
      });
      return Promise.all(accountsPromises);
    });
    await Promise.all(promises);
  }

  /**
   * Create Plaid Transaction custom objects
   * @param transactionsArray
   * @returns {Promise<unknown>}
   */
  async createTransactionsObjects(transactionsArray) {
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Plaid_Transaction__c')
        .create(transactionsArray, (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        });
    });
  }

  /**
   * Create Plaid Account Owner custom objects
   * @param {Owner|Array<Owner>} ownersArray
   * @returns {Promise<unknown>}
   */
  async createOwnersObjects(ownersArray) {
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Plaid_Account_Owner__c')
        .create(ownersArray, (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        });
    });
  }

  /**
   * Create Plaid Historical Balance custom objects
   * @param {Array<Object>} historicalBalanceArray
   * @returns {Promise<unknown>}
   */
  async createHistoricalBalanceObjects(historicalBalanceArray) {
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Plaid_Historical__c')
        .create(historicalBalanceArray, (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        });
    });
  }

  /**
   * Create Plaid account custom object
   * @param {Account|Array<Account>} account
   * @returns {Promise<unknown>}
   */
  async createPlaidAccountObject(account) {
    return new Promise((resolve, reject) => {
      this.conn.sobject('Plaid_Account__c').create(account, (err, ret) => {
        if (err) {
          return reject(err);
        }
        return resolve(ret);
      });
    });
  }

  /**
   * Push new data to salesforce
   * @param assetReportGetResponse
   * @param {string} applicationId Id of Application record in Salesforce
   * @returns {Promise<object>}
   */
  async pushReportData(assetReportGetResponse, applicationId) {
    const { report } = assetReportGetResponse;
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Plaid_Report__c')
        .create(new Report(report, applicationId), (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        });
    });
  }

  /**
   * Save user Token in Salesforce
   * @param {string} token
   * @param {string} applicationId
   * @returns {Promise<Object>}
   */
  async saveTokenForUserId(token, applicationId) {
    return new Promise((resolve, reject) => {
      this.conn.sobject('Plaid_Token__c').create(
        {
          created_at__c: new Date(),
          isValid__c: true,
          token__c: CryptoProvider.encrypt(token),
          updated_at__c: new Date(),
          ApplicationId__c: applicationId,
        },
        (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        }
      );
    });
  }

  /**
   * Create manual upload object for user_id
   * @param {string} applicationId Id of Application record in Salesforce
   * @returns {Promise<Object>}
   */
  async createManualUploadObject(applicationId) {
    return new Promise((resolve, reject) => {
      this.conn.sobject('Plaid_User_File__c').create(
        {
          ApplicationId__c: applicationId,
        },
        (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        }
      );
    });
  }

  /**
   * Upload file as attachment
   * @param {string} ParentId
   * @param {string} fileName
   * @param {Buffer} buffer
   * @param {string} ContentType
   * @param {string} [ObjectType=Attachment]
   * @returns {Promise<Object>}
   */
  async uploadFileAsAttachment(
    ParentId,
    fileName,
    buffer,
    ContentType,
    ObjectType = 'Attachment'
  ) {
    const base64data = buffer.toString('base64');
    return new Promise((resolve, reject) => {
      this.conn.sobject(ObjectType).create(
        {
          ParentId,
          Name: fileName,
          Body: base64data, // Base64
          ContentType,
        },
        (err, uploadedAttachment) => {
          if (err) {
            return reject(err);
          }
          return resolve(uploadedAttachment);
        }
      );
    });
  }

  /**
   * Get old (180 days) valid tokens
   * @param {number=15552000000} timestamp (180 days)
   * @returns {Promise<Object>}
   */
  async getOldValidTokens(timestamp = 1000 * 60 * 60 * 24 * 180) {
    const old = new Date(Date.now() - timestamp).toJSON();
    const oldLiteral = jsforce.Date.toDateTimeLiteral(old);
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Plaid_Token__c')
        .find(
          {
            isValid__c: true,
            updated_at__c: {
              $lte: oldLiteral,
            },
          },
          '*'
        )
        .execute((err, records) => {
          if (err) {
            return reject(err);
          }
          return resolve(records);
        });
    });
  }

  /**
   * Get client by token
   * @param {string} token
   * @returns {Promise<Array<Object>>}
   */
  async getClientByToken(token) {
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Application__c')
        .find(
          {
            token__c: token,
          },
          '*'
        )
        .limit(1)
        .execute((err, records) => {
          if (err) {
            return reject(err);
          }
          return resolve(records);
        });
    });
  }

  /**
   * Get client by userId
   * @param userId
   * @returns {Promise<Array<Object>>}
   */
  async getClientByUserId(userId) {
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Application__c')
        .find(
          {
            client_id__c: userId,
          },
          '*'
        )
        .limit(1)
        .execute((err, records) => {
          if (err) {
            return reject(err);
          }
          return resolve(records);
        });
    });
  }

  /**
   * Update token in Salesforce
   * @param {Object} data
   * @param {string|number} Id
   * @returns {Promise<void>}
   */
  async updateTokens(data, Id) {
    const objs = { ...data, Id };
    return new Promise((resolve, reject) => {
      this.conn.sobject('Plaid_Token__c').update(objs, (err, ret) => {
        if (err) {
          return reject(err);
        }
        return resolve(ret);
      });
    });
  }

  /**
   * Create Application record
   * @param {Object} applicationData
   * @returns {Promise<Object>}
   */
  async createApplication(applicationData) {
    return new Promise((resolve, reject) => {
      this.conn
        .sobject('Application__c')
        .create(applicationData, (err, ret) => {
          if (err) {
            return reject(err);
          }
          return resolve(ret);
        });
    });
  }

  /**
   * Delete Application record by Id
   * @param {string} recordId
   * @returns {Promise<Object>}
   */
  async deleteApplication(recordId) {
    return new Promise((resolve, reject) => {
      this.conn.sobject('Application__c').destroy(recordId, (err, ret) => {
        if (err) {
          return reject(err);
        }
        return resolve(ret);
      });
    });
  }
}

module.exports = SalesforceProvider;
