const crypto = require('crypto');
const logger = require('../common/Logger')('src/service/keysGenerator.js');

const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);

/**
 * Generate random keys for encryption/decryption.
 */
logger.debug('Generate random keys:');
logger.debug(`Key (base64): ${key.toString('base64')}`);
logger.debug(`iv (base64): ${iv.toString('base64')}`);
