/**
 * HistoricalBalance object for storing data in Salesforce
 */
class HistoricalBalance {
  /**
   * Constructor will use raw Account data from plaid assets endpoint
   * @param accountRaw Raw account Data from plaid assets endpoint
   * @param {string} applicationId Id of Application record in Salesforce
   * @param {string} accountId Id of Plaid Account record in Salesforce
   */
  constructor(accountRaw, applicationId, accountId) {
    this.historical_balances = accountRaw.historical_balances;
    this.applicationId = applicationId;
    this.accountId = accountId;
  }

  /**
   * Generating valid custom objects
   * @returns {{current_balance__c: *, date__c: *, unofficial_currency_code__c: *, account_id__c: (*|string), ApplicationId__c: string, iso_currency_code__c: *}[]}
   * @constructor
   */
  get Balances() {
    return this.historical_balances.map(balance => {
      return {
        current_balance__c: balance.current,
        date__c: balance.date,
        iso_currency_code__c: balance.iso_currency_code,
        unofficial_currency_code__c: balance.unofficial_currency_code,
        ApplicationId__c: this.applicationId,
        Plaid_Account_Id__c: this.accountId,
      };
    });
  }
}

module.exports = HistoricalBalance;
