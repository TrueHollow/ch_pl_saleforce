/**
 * Owner object for storing data in Salesforce
 */
class Owner {
  /**
   * Constructor will use raw Account data from plaid assets endpoint
   * @param accountRaw Raw account Data from plaid assets endpoint
   * @param {string} applicationId Id of Application record in Salesforce
   * @param {string} accountId Id of Plaid Account record in Salesforce
   */
  constructor(accountRaw, applicationId, accountId) {
    this.ownersJsonStructure__c = JSON.stringify(accountRaw.owners);
    this.ApplicationId__c = applicationId;
    this.Plaid_Account_Id__c = accountId;
  }
}

module.exports = Owner;
