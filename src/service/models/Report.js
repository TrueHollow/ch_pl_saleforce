/**
 * Report object for storing data in Salesforce
 */
class Report {
  /**
   * Constructor will use raw Response data from plaid assets endpoint
   * @param data Raw response Data from plaid assets endpoint
   * @param {string} applicationId Id of Application record in Salesforce
   */
  constructor(data, applicationId) {
    this.asset_report_id__c = data.asset_report_id;
    this.client_report_id__c = data.client_report_id;
    this.date_generated__c = data.date_generated;
    this.days_requested__c = data.days_requested;
    this.ApplicationId__c = applicationId;
  }
}

module.exports = Report;
