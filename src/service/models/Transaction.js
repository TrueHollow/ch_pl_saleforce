/**
 * Transaction object for storing data in Salesforce
 */
class Transaction {
  /**
   * Constructor will use raw Transaction data from plaid assets endpoint
   * @param tranRaw Raw Transaction Data from plaid assets endpoint
   * @param {string} applicationId Id of Application record in Salesforce
   * @param {string} accountId Id of Plaid Account record in Salesforce
   */
  constructor(tranRaw, applicationId, accountId) {
    this.account_id__c = tranRaw.account_id;
    this.amount__c = tranRaw.amount;
    this.date__c = tranRaw.date;
    this.origional_description__c = tranRaw.original_description;
    this.pending__c = tranRaw.pending;
    this.transaaction_id__c = tranRaw.transaction_id;
    this.ApplicationId__c = applicationId;
    this.Plaid_Account_Id__c = accountId;
  }
}

module.exports = Transaction;
