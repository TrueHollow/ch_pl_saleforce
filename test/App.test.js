const crypto = require('crypto');
const { readFileSync } = require('fs');
const path = require('path');
const chai = require('chai');
const faker = require('faker');
chai.use(require('chai-http'));

const { expect } = chai;
const { app } = require('../src/index');
const Backend = require('../src/service/Backend');
const logger = require('../src/common/Logger')('test/App.test.js');

const appToken = crypto.randomBytes(32).toString('hex');
const invalidToken = crypto.randomBytes(32).toString('hex');
const url = `/bind/${appToken}`;
let appRecordId;

describe('App test', () => {
  before(async () => {
    await Backend.init();
    const applicationStub = {
      client_id__c: faker.company.companyName(),
      email__c: faker.internet.email(),
      token__c: appToken,
    };
    const result = await Backend.SalesProvider.createApplication(
      applicationStub
    );
    appRecordId = result.id;
  });
  after(async () => {
    await Backend.SalesProvider.deleteApplication(appRecordId);
  });
  it('test GET method for / (404)', async () => {
    const res = await chai.request(app).get('/');
    expect(res).to.have.status(404);
  });
  it('test GET method for /bind/token endpoint', async () => {
    logger.debug(`Testing url (${url})`);
    const res = await chai.request(app).get(url);
    expect(res).to.have.status(200);
  });
  it('test GET method for /bind/token endpoint (with invalid token)', async () => {
    const invalidUrl = `/bind/${invalidToken}`;
    logger.debug(`Testing invalid token url (${invalidUrl})`);
    const res = await chai.request(app).get(invalidUrl);
    expect(res).to.have.status(404);
  });
  it('test POST method for /upload', async () => {
    const pathToFile = path.resolve(__dirname, 'etc', 'file_test.txt');
    const res = await chai
      .request(app)
      .post('/upload')
      .field('salesforceToken', appToken)
      .attach('userDocuments', readFileSync(pathToFile), 'file_test.txt');
    expect(res).to.have.status(200);
  });
  it('test POST method for /upload (invalid token 404)', async () => {
    const pathToFile = path.resolve(__dirname, 'etc', 'file_test.txt');
    const res = await chai
      .request(app)
      .post('/upload')
      .field('salesforceToken', invalidToken)
      .attach('userDocuments', readFileSync(pathToFile), 'file_test.txt');
    expect(res).to.have.status(404);
  });
  it('test POST assets endpoint', async () => {
    const res = await chai
      .request(app)
      .post('/assets/data')
      .type('form')
      .send({
        publicToken: '',
        salesforceToken: '',
      });
    expect(res).to.have.status(404);
  });
});
