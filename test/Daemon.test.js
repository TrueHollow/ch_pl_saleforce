const chai = require('chai');

const { expect } = chai;
const { processItem } = require('../src/daemon');
const Backend = require('../src/service/Backend');

const testObject = {
  attributes: {
    type: 'Plaid_Token__c',
    url: '/services/data/v42.0/sobjects/Plaid_Token__c/a0J3k00001JgDPpEAN',
  },
  Id: 'a0J3k00001JgDPpEAN',
  OwnerId: '0053k00000A4eRZAAZ',
  IsDeleted: false,
  Name: 'A-0005',
  CreatedDate: '2019-11-13T01:16:40.000+0000',
  CreatedById: '0053k00000A4eRZAAZ',
  LastModifiedDate: '2019-11-13T01:16:40.000+0000',
  LastModifiedById: '0053k00000A4eRZAAZ',
  SystemModstamp: '2019-11-13T01:16:40.000+0000',
  LastViewedDate: '2019-11-13T11:30:18.000+0000',
  LastReferencedDate: '2019-11-13T11:30:18.000+0000',
  token__c:
    'OIrKR6GRCxiJ9WxxEClgggYITsUJWAd1Hk8FznEX4RzEiWTi76c1/U9a2rA9w7ccvwXnq/bA1exmlRqIxQnfew==',
  created_at__c: '2019-11-13T01:16:39.000+0000',
  updated_at__c: '2019-11-13T01:16:39.000+0000',
  isValid__c: true,
  userId__c: 'test_client1',
};

describe('Daemon test', () => {
  before(async () => {
    await Backend.init();
  });
  after(async () => {});
  it('test process token', async () => {
    const { err, result } = await processItem(testObject);
    // eslint-disable-next-line no-unused-expressions
    expect(err).to.be.undefined;
    // eslint-disable-next-line no-unused-expressions
    expect(result).to.be.true;
  });
});
