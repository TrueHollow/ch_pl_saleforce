const crypto = require('crypto');
const faker = require('faker');
const puppeteer = require('puppeteer-extra');
const pluginStealth = require('puppeteer-extra-plugin-stealth');
const chai = require('chai');

puppeteer.use(pluginStealth());

const { expect } = chai;
const { listener } = require('../src/index');
const Backend = require('../src/service/Backend');
const logger = require('../src/common/Logger')('test/App.test.js');

let browser;
let appRecordId;

const puppeteerConfig = {
  args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--disable-infobars',
    '--window-position=0,0',
    '--ignore-certifcate-errors',
    '--ignore-certifcate-errors-spki-list',
    '--lang=en-US',
    '--disable-features=site-per-process',
  ],
  headless: true,
  ignoreHTTPSErrors: true,
  slowMo: 20,
};

const GOTO_OPTIONS = { waitUntil: 'networkidle0' };

function getRandomInt(min, max) {
  const minimum = Math.ceil(min);
  const maximum = Math.floor(max);
  return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}

const TYPE_MINIMUM_DELAY = 30;
const TYPE_MAXIMUM_DELAY = 50;

const createInputDelay = () => {
  return {
    delay: getRandomInt(TYPE_MINIMUM_DELAY, TYPE_MAXIMUM_DELAY),
  };
};

const appToken = crypto.randomBytes(32).toString('hex');

const checkClickAndDelay = async (page, selector) => {
  await page.waitFor(2000);
  await page.waitForSelector(selector);
  await page.click(selector, createInputDelay());
};

describe('Ui test', () => {
  before(async () => {
    [browser] = await Promise.all([
      await puppeteer.launch(puppeteerConfig),
      Backend.init(),
    ]);
    const applicationStub = {
      client_id__c: faker.company.companyName(),
      email__c: faker.internet.email(),
      token__c: appToken,
    };
    const result = await Backend.SalesProvider.createApplication(
      applicationStub
    );
    appRecordId = result.id;
  });
  after(async () => {
    const promises = [Backend.SalesProvider.deleteApplication(appRecordId)];
    if (browser) {
      promises.push(browser.close());
    }
    await Promise.all(promises);
  });
  it('test User auth', async () => {
    const page = await browser.newPage();
    page.on('console', msg =>
      logger.debug('PAGE LOG:', msg.type(), msg.text())
    );
    await page.setViewport({ width: 1920, height: 1040 });
    const { port } = listener.address();
    const url = `http://localhost:${port}/bind/${appToken}`;
    await page.goto(url, GOTO_OPTIONS);

    // Wait for iframe to be rendered.
    await page.waitForSelector('iframe#plaid-link-iframe-1');

    // Button "Connect with Plaid"
    await checkClickAndDelay(page, '#link-btn');

    // Button "Continue"
    const iframeHandle = await page.$('iframe#plaid-link-iframe-1');
    const frame = await iframeHandle.contentFrame();
    await checkClickAndDelay(frame, 'button.Button.Button--is-plaid-color');

    // Button "CHASE"
    await checkClickAndDelay(frame, 'li.InstitutionSelectPaneButton');

    // Clicking on login input
    await checkClickAndDelay(frame, '.Input__field');

    // Login
    await page.keyboard.type('user_good', createInputDelay());
    await page.keyboard.press('Tab', createInputDelay());
    await page.keyboard.type('pass_good', createInputDelay());
    await page.keyboard.press('Enter', createInputDelay());

    // Click on "Allow"
    await checkClickAndDelay(frame, 'button.Button');

    // Waiting until backend will finish
    await page.waitForNavigation(GOTO_OPTIONS);

    const resultUrl = await page.url();
    const check = resultUrl.endsWith('/link_success');
    expect(check).to.be.true;
  });
});
